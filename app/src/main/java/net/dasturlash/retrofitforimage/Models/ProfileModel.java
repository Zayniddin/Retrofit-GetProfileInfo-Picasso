package net.dasturlash.retrofitforimage.Models;

/**
 * Created by Zayniddin on 20-Oct-17.
 */

public class ProfileModel {
    private String gender;
    private ProfileName name;
    private ProfileLocation location;
    private String email;
    private ProfileLogin login;
    private String dob;
    private String registered;
    private String phone;
    private String cell;
    private ProfileId id;
    private ProfilePicture picture;
    private String nat;

    public ProfileModel(String gender, ProfileName name, ProfileLocation location, String email, ProfileLogin login, String dob, String registered, String phone, String cell, ProfileId id, ProfilePicture picture, String nat) {
        this.gender = gender;
        this.name = name;
        this.location = location;
        this.email = email;
        this.login = login;
        this.dob = dob;
        this.registered = registered;
        this.phone = phone;
        this.cell = cell;
        this.id = id;
        this.picture = picture;
        this.nat = nat;
    }

    public String getGender() {
        return gender;
    }

    public ProfileName getName() {
        return name;
    }

    public ProfileLocation getLocation() {
        return location;
    }

    public String getEmail() {
        return email;
    }

    public ProfileLogin getLogin() {
        return login;
    }

    public String getDob() {
        return dob;
    }

    public String getRegistered() {
        return registered;
    }

    public String getPhone() {
        return phone;
    }

    public String getCell() {
        return cell;
    }

    public ProfileId getId() {
        return id;
    }

    public String getPicture() {
        return picture.getLarge();
    }

    public String getNat() {
        return nat;
    }

    @Override
    public String toString() {
        return " " +
                " \nFirst name : " + name.getFirst() +
                " \n\nLast name : " + name.getLast() +
                " \n\nGender : " + gender +
                " \n\nLocation : " + location +
                " \n\nEmail : '" + email +
                " \n\nLogin : " + login +
                //", dob='" + dob + '\'' +
                //", registered='" + registered + '\'' +
                " \n\nPhone : '" + phone + '\'' +
                " \n\nCell: '" + cell +
                " \n\nId : " + id +
               // ", picture=" + picture +
                " \n\nNationality : '" + nat;
    }
}
