package net.dasturlash.retrofitforimage.Models;

/**
 * Created by Zayniddin on 20-Oct-17.
 */

public class ProfileLogin {
    private String username;
    private String password;
    private String salt;
    private String md5;
    private String sha1;
    private String sha26;

    public ProfileLogin(String username, String password, String salt, String md5, String sha1, String sha26) {
        this.username = username;
        this.password = password;
        this.salt = salt;
        this.md5 = md5;
        this.sha1 = sha1;
        this.sha26 = sha26;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public String getSalt() {
        return salt;
    }

    public String getMd5() {
        return md5;
    }

    public String getSha1() {
        return sha1;
    }

    public String getSha26() {
        return sha26;
    }
}
