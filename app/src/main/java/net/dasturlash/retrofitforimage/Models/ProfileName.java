package net.dasturlash.retrofitforimage.Models;

/**
 * Created by Zayniddin on 20-Oct-17.
 */

public class ProfileName {
    private String title;
    private String first;

    public ProfileName(String title, String first, String last) {
        this.title = title;
        this.first = first;
        this.last = last;
    }

    public String getTitle() {
        return title;
    }

    public String getFirst() {
        return first;
    }

    public String getLast() {
        return last;
    }

    private String last;


}
