package net.dasturlash.retrofitforimage.Models;

/**
 * Created by Zayniddin on 20-Oct-17.
 */

public class ProfileId {
    private String id;
    private String value;

    public ProfileId(String id, String value) {
        this.id = id;
        this.value = value;
    }

    public String getId() {
        return id;
    }

    public String getValue() {
        return value;
    }
}
