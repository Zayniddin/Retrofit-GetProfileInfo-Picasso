package net.dasturlash.retrofitforimage.Api;

import net.dasturlash.retrofitforimage.Models.ProfileModel;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by Zayniddin on 19-Oct-17.
 */

public interface ApiInterface {

    //@GET("/users/{user}/repos") Call<List<Model>> reposForUser(@Path("user") String user);

    @GET("api/getProfileInfo") Call<ProfileModel> fx();

}
