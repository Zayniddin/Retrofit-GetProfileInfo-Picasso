package net.dasturlash.retrofitforimage.Activities;

import android.media.Image;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import net.dasturlash.retrofitforimage.Api.ApiClient;
import net.dasturlash.retrofitforimage.Api.ApiInterface;
import net.dasturlash.retrofitforimage.Models.ProfileModel;
import net.dasturlash.retrofitforimage.R;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    private ProfileModel myProfile;
    private ApiInterface apiInterface;
    private Call<ProfileModel> profileCall;

    private ImageView imgView;
    private TextView tv;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        tv = (TextView) findViewById(R.id.textView);
        getData();
    }

    public void getData() {
        apiInterface = ApiClient.getClient().create(ApiInterface.class);

        profileCall = apiInterface.fx();

        profileCall.enqueue(new Callback<ProfileModel>() {
            @Override
            public void onResponse(Call<ProfileModel> call, Response<ProfileModel> response) {
                myProfile = response.body();
                Toast.makeText(MainActivity.this, "Data are loaded", Toast.LENGTH_SHORT).show();
                imgView = (ImageView) findViewById(R.id.imageView);

                Picasso.with(MainActivity.this).load(myProfile.getPicture())
                        .into(imgView);

                setText();
                Toast.makeText(MainActivity.this, "ok", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Call<ProfileModel> call, Throwable t) {
                Toast.makeText(MainActivity.this, "Error is occured: " + t.getMessage(), Toast.LENGTH_LONG).show();
                tv.setText("" + t.getMessage());
            }
        });
    }

    public void setText() {
        tv.setText(myProfile.toString());
    }
}